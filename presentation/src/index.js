import React from 'react';

import {
  Appear, BlockQuote, Cite, CodePane, Deck, Fill,
  Heading, Layout, Link, ListItem, List, Quote, Slide,
  Spectacle,
} from 'spectacle';

import CodeSlide from 'spectacle-code-slide';

import createTheme from 'spectacle/lib/themes/default';

require('normalize.css');
require('spectacle/lib/themes/default/index.css');

const theme = createTheme({
  primary: 'rgb(60, 60, 60)',
  secondary: 'hotpink',
  tertiary: 'white',
  quartenary: '#8BC34A',
});
theme.screen.components.list.listStylePosition = 'outside';
theme.screen.components.list.marginRight = '40px';
theme.screen.components.codePane.pre.margin = '20px auto';
theme.screen.components.codePane.pre.fontSize = '1.1rem';
theme.screen.components.heading.h3.color = '#8BC34A'; // FIXME: quartenary

const mkColumn = item =>
  <Appear>
    <Fill>
      {item}
    </Fill>
  </Appear>;

const AppearingColumns = ({ children }) =>
  <Layout>
    {children.map(mkColumn)}
  </Layout>;
AppearingColumns.propTypes = {
  children: React.PropTypes.node,
};

export default () =>
  <Spectacle theme={theme}>
    <Deck transition={['zoom', 'slide']} transitionDuration={500} progress="bar">
      <Slide>
        <Heading size={1} fit>Demystifying</Heading>
        <Heading size={2} fit textColor="secondary">Functional Programming</Heading>
        <Heading size={3}>Deck 1 of π±ε</Heading>
      </Slide>
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`// The Y combinator is defined as:
// Y          = λf.(λx.f (x x))(λx.f (x x))
// The Z combinator is defined as:
// Z          = λf.(λx.f (λv.((x x) v))) (λx.f (λv.((x x) v)))

const Z       = f => (x => f (v => ((x (x)) (v)))) (x => f (v => ((x (x)) (v))))

// example of usage: factorial
const factgen = fact => n => n < 2 ? 1 : n * fact(n-1)
const fact    = Z(factgen)`
      }
        ranges={[
              { loc: [0, 10], title: 'FP is fun!' },
              { loc: [0, 4], title: 'Simple idea' },
              { loc: [5, 6], title: 'Easy implementation' },
              { loc: [7, 10], title: 'Intuitive application' },
        ]}
      />
      <Slide bgColor="secondary">
        <BlockQuote>
          <Quote>Point-free idempotent comonoidal functors, due to their referential transparency and identity-preservation properties, are often used for composition of partially applied HOFs.</Quote>
          <Cite>💕, Your Local Curry Club</Cite>
        </BlockQuote>
      </Slide>
      <Slide>
        <Heading size={1} fit>What you %s see in this presentation</Heading>
        <Layout>
          <Fill>
            <Heading size={3}>will</Heading>
            <List>
              <ListItem>'bit programming basics people often miss</ListItem>
              <ListItem>FP 101.5</ListItem>
              <ListItem>An attempt to make things flow smoothly to each other</ListItem>
              <ListItem>Focus on some tricky bits often gotten subtly wrong</ListItem>
            </List>
          </Fill>
          <Fill>
            <Heading size={3}>won't</Heading>
            <List>
              <ListItem>Much "why"</ListItem>
              <ListItem>Immutability (only in passing)</ListItem>
              <ListItem>Monads, Lifts and Cofunctors</ListItem>
              <ListItem>Anymore λ-calculus</ListItem>
            </List>
          </Fill>
        </Layout>
      </Slide>
      <Slide>
        <Heading size={1} fit>Statement vs. Expression</Heading>
        <AppearingColumns>
          <CodePane
            lang="python"
            source={
`x = 0

def f():
    # global x
    x = 10

f()`
    }
          />
          <CodePane
            lang="python"
            source={
`def f():
    return 10

x = f()`
    }
          />
        </AppearingColumns>
        <AppearingColumns>
          <CodePane
            lang="js"
            source={
`let a;
if (p) { a = 5; } else { a = 10; }`
    }
          />
          <CodePane
            lang="rust"
            source={'let a = if (p) { 5 } else { 10 };'}
          />
        </AppearingColumns>
      </Slide>
      <Slide>
        <Heading size={1}>Side Effects</Heading>
        <Heading size={5} textColor="secondary">And Pure Functions</Heading>
        <AppearingColumns>
          <CodePane
            lang="python"
            source={
`x = 0

def f():
    # global x
    x = 10

f()`
    }
          />
          <CodePane
            lang="python"
            source={
`def f():
    return 10

x = f()`
    }
          />
        </AppearingColumns>
        <AppearingColumns>
          <CodePane
            lang="python"
            source={
`def f():
    return sys.version * 2

x = f()`
    }
          />
          <CodePane
            lang="python"
            source={
`def f(version):
    return version * 2

x = f(sys.version)`
    }
          />
        </AppearingColumns>
      </Slide>
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`(a) => a
f = (a) => a
function f(a) { return a; }

x = 5
(() => x)() // x
() => { x = 10 }
function f() { x = 10; }`
      }
        ranges={[
              { loc: [0, 8], title: 'Lambdas vs. Closures' },
              { loc: [0, 1], title: 'Make a lambda', note: 'AKA a anonymous function' },
              { loc: [1, 2], title: 'Name it', note: 'i.e. name an anonymous function [sic]' },
              { loc: [1, 3], title: 'Identical' },
              { loc: [4, 6], title: 'Make a closure', note: 'Can be *any* function which operates on context' },
              { loc: [6, 7], title: 'Change context', note: 'Impure by design!' },
              { loc: [7, 8], title: 'A named closure' },
        ]}
      />
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`(a) => a
(a, b) => a + b
(a, b, c) => a + b + c

-a ≡ sub(0, a)
a + b ≡ add(a, b)
p ? a : b ≡ if (p) a else b`
      }
        ranges={[
              { loc: [0, 10], title: 'Function Arity' },
              { loc: [0, 1], title: 'Unary' },
              { loc: [1, 2], title: 'Binary' },
              { loc: [2, 3], title: 'Ternary', note: 'Ternary operation anyone?' },
              { loc: [4, 5], title: 'Unary' },
              { loc: [5, 6], title: 'Unary' },
              { loc: [6, 7], title: 'Ternary', note: 'Ternary operation everyone!' },
        ]}
      />
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`def f1(s):
    acc = []
    for x in s:
        acc += x * 2
    return acc

f = (a) => a * 2
def f2(f, s):
    acc = []
    for x in s:
        acc += f(x)
    return acc

f = (a) => a * 2
map(f, s)

map((a) => a * 2, s)`
      }
        ranges={[
              { loc: [0, 20], title: 'Higher-Order Functions' },
              { loc: [0, 5], title: 'Do smth with list elements', note: 'The "usual" way' },
              { loc: [6, 12], title: 'Do smth with list elements', note: 'The HOF way' },
              { loc: [13, 15], title: 'Do smth with list elements', note: 'The Functional way' },
              { loc: [16, 17], title: 'Do smth with list elements', note: 'The one-liner' },
        ]}
      />
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`add = (a, b) => a + b
f = add(5)
// f = (x) => add(5, x)
f(10) ≡ add(5)(10) ≡ add(5, 10)

add3 = (a, b, c) => a + b + c
add3(1)(2)(3) ≡ add3(1)(2, 3)

(==)
(==) 6
(==) 6 9`
      }
        ranges={[
              { loc: [0, 11], title: 'Partial Application & Currying' },
              { loc: [1, 3], note: '*The* partial application' },
              { loc: [1, 4], note: 'We can substitute named function with its body (referential transparency)' },
              { loc: [5, 7], note: 'NB: left-hand is curried, right-hand is *only* partially applied' },
              { loc: [8, 11], note: 'Works with operators just as well' },
        ]}
      />
      <Slide>
        <Heading size={1}>std::HOF</Heading>
        <AppearingColumns>
          <CodePane lang="js" source={'filter(p, s)'} />
          <CodePane lang="js" source={'map(f, s)'} />
          <CodePane lang="js" source={'reduce(f, s[, acc])'} />
        </AppearingColumns>
        <AppearingColumns>
          <CodePane
            lang="python" source={
`def filter(p, s):
    acc = []
    for x in s:
        if p(x):
            acc += x
    return acc`}
          />
          <CodePane
            lang="python" source={
`def map(f, s):
    acc = []
    for x in s:
        acc += f(x)
    return acc`}
          />
          <CodePane
            lang="python" source={
`def reduce(f, s, acc=0):
    for x in s:
        acc = f(acc, x)
    return acc`}
          />
        </AppearingColumns>
        <Appear>
          <CodePane margin="0px auto" lang="js" source={'filter (>= 5) (1, 3, 5)   // (5)'} />
        </Appear>
        <Appear>
          <CodePane margin="0px auto" lang="js" source={'map    (** 2) (5, 10, 15) // (25, 100, 225)'} />
        </Appear>
        <Appear>
          <CodePane margin="0px auto" lang="js" source={'reduce (*)    (2, 5, 1)   // 10'} />
        </Appear>
      </Slide>
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`f'(x) = f(g(x))
f''(x) = f(g(h(i(j(k(l(x)))))))

f' = f . g
f'' = f . g . h . i . j . k . l

f'(5)

map (x => x - 2) (3, 4, 5)
map (-2) (3, 4, 5)`
      }
        ranges={[
              { loc: [0, 10], title: 'Point-Free Style' },
              { loc: [0, 1], title: 'Your usual way' },
              { loc: [1, 2], title: 'Braces explosion!' },
              { loc: [3, 5], title: 'Point-Free function composition', note: '...actually has dots!' },
              { loc: [6, 7], title: 'Use as usual' },
              { loc: [8, 10], title: 'Kind of PFS too', note: "We don't have to make an additional lambda here" },
        ]}
      />
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`pr0n = [{
    'url': 'https://example.com/123',
    'favs': 146
    },
    ...
    ]

// download(path, object)
download_to_hdd = download('/media/pr0n')

good_pr0n = filter(x => x.favs > 200)(pr0n)
good_pr0n_favcount = reduce((x, acc) => acc + x.favs)(good_pr0n)

if good_pr0n_favcount > 100500 {
    map(x => download_to_hdd(x), good_pr0n)
}`
      }
        ranges={[
              { loc: [0, 16], title: 'Example time!', note: 'The half-imperative way' },
              { loc: [0, 6], title: 'We have a %pr0nsite% dump', note: 'And are preparing for Cheburashka...' },
              { loc: [7, 9], note: 'Partial application' },
              { loc: [10, 11], note: 'We only need well-received pr0n: 200 favs+' },
              { loc: [11, 12], note: 'Count sum of favs' },
              { loc: [13, 16], note: 'Big enough? Download!' },
              { loc: [10, 16] },
        ]}
      />
      <CodeSlide
        transition={[]}
        lang="js"
        code={
`pr0n = [{
    'url': 'https://example.com/123',
    'favs': 146
    },
    ...
    ]

download_to_hdd = download('/media/pr0n')

function return_pr0n_if_nice(pr0n) {
    if reduce((x, acc) => acc + x.favs)(pr0n) > 201000 { return pr0n; }
}

(map(download_to_hdd) .
 return_pr0n_if_nice .
 filter(x => x.favs > 200)) pr0n`
      }
        ranges={[
              { loc: [0, 16], title: 'Example time!', note: 'The more functional way' },
              { loc: [0, 6] },
              { loc: [9, 16] },
              { loc: [9, 12], note: 'This calls for a monad...' },
        ]}
      />
      <Slide>
        <Heading fit size={1}>Questions?</Heading>
        <Heading fit size={3} textColor="quartenary">Shall anyone need me,</Heading>
        <Link href="mailto:wldhx+dfp1@wldhx.me"><Heading fit size={2} textColor="secondary">wldhx+dfp1@wldhx.me</Heading></Link>
        <Heading fit size={6} textColor="secondary">FB52 7CDA C117 6535 A4CF 9B4C 0E8C B6EC 231E DDFE</Heading>
      </Slide>
    </Deck>
  </Spectacle>;
