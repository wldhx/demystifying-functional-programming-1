# [Demystifying Functional Programming I](https://wldhx.gitlab.io/demystifying-functional-programming-1)

An hour-ish-long talk on FP explaining some basic concepts (and getting them right: special focus is put on things often gotten subtly wrong, like currying vs partial application) and trying not to scare people with jargon too much (see slide 4 for more things in and not in here). First in planned series.

The "demystifying" in the title is about how FP has a reputation for being a bafflingly monadic weird academical thingy: this talk introduces functional concepts gradually, basing them on things familiar to your median programmer and, in a sense, dispels the myth.

Following talks are planned to gently introduce λ-calculus and how one can profit from it and guide one to write some actual functional code (which calls for error handling, IO, etc.).

## Building

To build a local version of this deck, use

```sh
git clone https://gitlab.com/wldhx/demystifying-functional-programming-1
cd demystifying-functional-programming-1
yarn install
npm start # to start a local dev server
npm run build # to build a static version
```
